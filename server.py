import socket
import struct
from colorama import Fore, Back, Style

HOST = '127.0.0.1'
PORT = 4444

def formatdata(data):
    return data.encode('utf-8')


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    
    print('Connected by', addr)
    input()
    while True:
        print('Sending...')
        conn.sendall(formatdata(Style.RESET_ALL + 'hello world! hello world! hello world! hello world! hello world! hello world! hello world! hello world! \r\n'))
        conn.sendall(formatdata(Fore.RED + 'good bye!\r\n'))
        input()
