from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QScrollBar, QLineEdit, QPushButton
from PyQt5.QtCore import QRegExp, Qt, QByteArray
from QTermWidget import QTermWidget
import os
from colorama import Fore, Back, Style
#from qt_material import apply_stylesheet


QTermWidget.addCustomColorSchemeDir(r'C:\Program Files (x86)\qtermwidget2\share\qtermwidget5\color-schemes')


class Terminal(QTermWidget):
    def __init__(self):
        super().__init__(1)
        self.setColorScheme("Solarized")
        self.setFont(QtGui.QFont("Consolas", 14))
        self.setTerminalFont(QtGui.QFont("Consolas", 14))
        self.toggleShowSearchBar()
        self.show()

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.layout0 = QVBoxLayout(self)

        self.textbox = QLineEdit()
        self.button = QPushButton('Search')
        self.button.clicked.connect(self.buttonClicked)
        self.button2 = QPushButton('Add Text')
        self.button2.clicked.connect(self.addTextClicked)
        self.layout1 = QHBoxLayout(self)
        self.layout1.addWidget(self.textbox)
        self.layout1.addWidget(self.button)
        self.layout1.addWidget(self.button2)
        self.layout0.addLayout(self.layout1)
        
        self.scrollbar = QScrollBar()
        self.scrollbar.valueChanged.connect(self.scrollValueChanged)
        self.term = Terminal()
        self.term.scrollValuesChanged.connect(self.termScrollValuesChanged)
        self.term.configureRequest.connect(self.configureRequest)
        self.layout2 = QHBoxLayout(self)
        self.layout2.addWidget(self.term)
        self.layout2.addWidget(self.scrollbar)
        self.layout0.addLayout(self.layout2)
        
        self.setLayout(self.layout0)
        self.show()
    
    def addTextClicked(self):
        data = Style.RESET_ALL + 'hello world! hello world! hello world! hello world! hello world! hello world! hello world! hello world! \r\n'
        self.term.writeData(QByteArray(data.encode('utf-8')))
        
        data = Fore.RED + 'good bye!\r\n'
        self.term.writeData(QByteArray(data.encode('utf-8')))
    
    def configureRequest(self, point):
        self.term.copyClipboard()
        self.term.clearSelection()
        print(point)
    
    def buttonClicked(self):
        regExp = QRegExp(self.textbox.text())
        regExp.setPatternSyntax(QRegExp.FixedString)
        regExp.setCaseSensitivity(Qt.CaseInsensitive)
        self.term.searchFor(regExp)
        print('search: '+self.textbox.text())
    
    def scrollValueChanged(self, value):
        self.term.scrollBarPositionTrackChanged(value, value == self.scrollbar.maximum())
    
    def termScrollValuesChanged(self, maximum, page, value):
        self.scrollbar.setRange(0, maximum);
        self.scrollbar.setSingleStep(1);
        self.scrollbar.setPageStep(page);
        self.scrollbar.setValue(value);


app = QtWidgets.QApplication([])
#apply_stylesheet(app, theme='dark_teal.xml')
window = MainWindow()
app.exec()
